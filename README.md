Here's a couple of quick Google Chrome extensions designed to help with local web development.

When you type something in the omnibox and it doesn't have a DNS record or match a known TLD, Chrome's default behaviour is not to try and navigate to the URL but to take you to a search page with the URL as a search term. This is annoying if you constantly need to do things like access sites via entries in the local hosts file, or when you have a machine domain name with an unusual TLD. 

###Installation

- Open up extensions config page: `chrome://extensions/`
- Make sure Developer Mode is checked.
- Drag the two directories `direct_navigate_http` and `direct_navigate_https` onto the extensions config page.
- Enable them.

###Usage

- Type `ht <space>` followed by the URL for `http://`
- Type `hs <space>` followed by the URL for `https://`

###Why two separate extensions?

Because I wanted to use two separate keywords `ht` and `hs` depending on whether the intended URI was http or https, and you can't have multiple keywords per extension.

###To dos

- Don't blank out the omnibox when the request is made. Leave it showing the URL requested.
- If http response is 504 (unknown host), then assume the next thing typed into the omnibox is also intended to be a direct request rather than search term. At the moment, you need to remember to hit `ht` (or `hs`) before every single request or it'll take you to search.

###License

Released under WTFPL - see http://en.wikipedia.org/wiki/WTFPL